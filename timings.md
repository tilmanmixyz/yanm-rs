| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `./target/release/rmap` | 6.415 ± 0.004 | 6.410 | 6.419 | 1.40 ± 0.04 |
| `nmap` | 4.595 ± 0.133 | 4.372 | 4.693 | 1.00 |
