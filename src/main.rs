use std::net::ToSocketAddrs;

mod ports; mod cli;
use std::sync::Arc;

use clap::Parser; use ports::COMMON_PORTS; use tokio::io::AsyncWriteExt;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum PortMode {
    All,
    Common,
    Min,
}

fn get_ports(count: PortMode) -> Vec<u16> {
    use PortMode as P;
    // if  P::All {
    //     return (1..=u16::MAX).into_iter().collect();
    // } else {
    //     return COMMON_PORTS.to_owned().into_iter().collect();
    // }
    return match count {
        P::All => (1..=u16::MAX).into_iter().collect(),
        P::Common => COMMON_PORTS.to_owned().into_iter().collect(),
        P::Min => (1..=1000).into_iter().collect(),
    };
}

/// timeout takes milliseconds
///
/// Return the port number and the state (open, closed)
async fn scan_single_port(target: std::net::IpAddr, port: u16, timeout: u64) -> (u16, bool){
    let timeout = std::time::Duration::from_millis(timeout);
    let addr = match target {
        std::net::IpAddr::V4(v4addr) => {
            std::net::SocketAddr::V4(std::net::SocketAddrV4::new(v4addr, port))
        }
        std::net::IpAddr::V6(v6addr) => {
            std::net::SocketAddr::V6(std::net::SocketAddrV6::new(v6addr, port, 0, 0))
        }
    };

    return match tokio::time::timeout(timeout, tokio::net::TcpStream::connect(&addr)).await {
        Ok(stream) => {
            if let Ok(stream) = stream {
                if stream.writable().await.is_ok() {
                   return (port, true) 
                }
            }
            (port, false)
        }
        Err(_) => (port, false),
    };
}

/// timeout is in milli seconds
///
/// reurn open ports in a dynamic array
async fn scan(target: std::net::IpAddr, options: PortMode, concurrency: usize, timeout: u64) -> Vec<u16> {
    let ports = get_ports(options);
    let ports_len = ports.len();
    let ports = Arc::new(ports);


    let mut handles = Vec::with_capacity(concurrency);
    let (sender, receiver) = loole::unbounded();

    let ports_to_handle = ports_len / concurrency;

    for handle_num in 0..concurrency {
        let sender = sender.clone();
        let ports = ports.clone();
        let start = handle_num * ports_to_handle;
        let end = if (handle_num + 1) * ports_to_handle >= ports_len {
           ports_len
        } else {
            (handle_num + 1) * ports_to_handle
        };
        let handle = tokio::task::spawn(async move {
            let ports_range = &ports[start..end];
            let mut ports_open = Vec::new();
            for port in ports_range {
                match scan_single_port(target, *port, timeout).await {
                    (port, true) => _ = {
                        ports_open.push(port);
                    },
                    (_, false) => {}

                }
            }
            let _ = sender.send(ports_open);
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.await.unwrap();
    }

    let mut ports_open = Vec::new();
    (0..concurrency).into_iter().for_each(|_| {
        let foo = receiver.recv().unwrap();
        ports_open.extend_from_slice(&foo[..]);
    });
    ports_open.sort();
    ports_open
}

#[tokio::main]
async fn main() {
    let cli = cli::Cli::parse();

    let mut ports = PortMode::Min;
    if cli.common() {
        ports = PortMode::Common;
    }
    if cli.all() {
        ports = PortMode::All;
    }

    let url = cli.target();
    let socket_url = format!("{}:0", url);

    let socketaddrs = match socket_url.to_socket_addrs() {
        Ok(addrs) => addrs,
        Err(_) => {
            eprintln!("ERROR: Failed to resolve domain {url}");
            std::process::exit(1);
        }
    }.collect::<Vec<_>>();
    if socketaddrs.is_empty() {
        eprintln!("ERROR: not ip address found for domain {url}");
        std::process::exit(1);
    }
    let ip = socketaddrs[0].ip();
    let open_ports = scan(ip, ports, 8, 50).await;
    let mut output = String::with_capacity(open_ports.len() * 15);
    if !open_ports.is_empty() {
        output.push_str("PORT      STATE\n");
    }
    for port in open_ports {
        output.push_str(format!("{port}/tcp ").as_str());
        output.push_str(&" ".repeat(5 - port.to_string().len()));
        output.push_str("open\n");
    }
    let _ = tokio::io::stdout().write(&output.as_bytes()).await;
}
