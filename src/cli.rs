#[derive(clap::Parser, Debug, Eq, PartialEq)]
pub struct Cli {
    /// The target
    target: String,

    #[clap(short, long)]
    /// scan common ports
    common: bool,
    #[clap(short, long)]
    /// scan all ports, priority over common(-c)
    all: bool,
}

impl Cli {
    pub fn all(&self) -> bool {
        self.all
    }

    pub fn target<'a>(&'a self) -> &'a str {
        self.target.as_str()
    }

    pub fn common(&self) -> bool {
        self.common
    }
}
