# yanm - **Y**et **A**nother **N**etwork **M**apper

> scan a machine for open ports

## Building
```sh
git clone https://codeberg.org/tilmanmixyz/yanm-rs yanm
cd yanm
cargo build --release
# running
./target/release/yanm
```

## Usage

```sh
Usage: yanm [OPTIONS] <TARGET>

Arguments:
  <TARGET>  The target

Options:
  -c, --common  scan common ports
  -a, --all     scan all ports, priority over common(-c)
  -h, --help    Print help
```

### Options
- `-a`: scan all ports, priority over -c
- `-c`: scan common ports, as defined by nmap

### Arguments

- `<TARGET>`, the target, can be a ip address, v4 or v6, or a domain

## Copyright and License notice
© 2023 Tilman Andre Mix

Licensed under The [GNU General Public License v2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html#SEC1) or [LICENSE](LICENSE)

## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
